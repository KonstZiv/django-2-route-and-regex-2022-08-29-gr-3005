from django.http import HttpResponse, HttpRequest

def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Rout_1 is here')

def special_case_2003(request: HttpRequest) -> HttpResponse:
    return HttpResponse('специальный случай 2003')

def year_archive(requests: HttpRequest, year: int | None) -> HttpResponse:
    return HttpResponse(f'архив за год: {year}')

def month_archive(requests: HttpRequest, year: int | None, month: int | None) -> HttpResponse:
    return HttpResponse(f'архив за год: {year} и месяц {month}')

def article_detail(requests: HttpRequest, year: int | None, month: int | None, slug: str | None) -> HttpResponse:
    return HttpResponse(f'архив за год: {year} и месяц {month} \n название: {slug}')
